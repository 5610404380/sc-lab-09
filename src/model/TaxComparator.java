package model;

import java.util.Comparator;

import interfaces.Taxable;

public class TaxComparator implements Comparator<Taxable>{

	public int compare(Taxable other, Taxable other1) {
		
		// TODO Auto-generated method stub
		if(other.getTax()<other1.getTax()){
			return -1;
			
		}
		else if(other.getTax()>other1.getTax()){
			return 1;
			
		}
		else{
			return 0;
		}
	
	}
}
