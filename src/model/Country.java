package model;

import interfaces.Measurable;

public class Country implements Measurable {
	String name;
	double pop;
	public Country(String name, int pop){
		this.name = name;
		this.pop = pop;
	}
	@Override
	public double getMeasure() {
		return this.pop;
	}

}
