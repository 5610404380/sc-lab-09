package model;

import interfaces.Measurable;

public class BankAccount implements Measurable {
	String name;
	double balance;
	public BankAccount(String name, int balance) {
		this.name = name;
		this.balance = balance;
	}
	@Override
	public double getMeasure() {
		return this.balance;
	}

}
