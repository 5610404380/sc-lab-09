package model;

import interfaces.Taxable;

public class Product implements  Taxable,Comparable{
	String name;
	double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}
	@Override
	public double getTax() {
		double tax = price*7/100;
		return tax;
	}
	@Override
	public int compareTo(Object o) {
		Product ot = (Product) o;
		// TODO Auto-generated method stub
		if(this.price<ot.price){
			return -1;
			
		}
		else if(this.price>ot.price){
			return 1;
			
		}
		else{
			return 0;
		}
	
	}
	public String toString(){
		return "Name : "+this.name+" Price : "+this.price+
				" Tax : "+this.getTax();
	}
	}


