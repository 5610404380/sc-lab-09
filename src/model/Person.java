package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable , Taxable , Comparable {
	String name;
	double height;
	double salary;
	
	public Person(String name, double height, double salary){
		this.name = name;
		this.height = height;
		this.salary = salary;
	}
	
	public double getMeasure(){
		return this.height;
	}

	@Override
	public double getTax() {
		double tax = 0;
		double annual = 0;
		if(Math.floor(this.salary) <= 300000){
			tax = this.salary * 0.05;
		}
		else{
			tax = 300000 * 0.05;
			annual = this.salary - 300000;
			tax = tax + (annual * 0.1);
		}
		return tax;
	}

	@Override
	public int compareTo(Object o) {
		Person per = (Person) o;
		// TODO Auto-generated method stub
		if(this.salary<per.salary){
			return -1;
			
		}
		else if(this.salary>per.salary){
			return 1;
			
		}
		else{
			return 0;
		}
	
	}
	
	public String toString(){
		return "Name : "+this.name+" Height : "+this.height+" salary : "+this.salary+
				" Tax : "+this.getTax();
}
}
