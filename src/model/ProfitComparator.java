package model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company o, Company o1) {
		
		// TODO Auto-generated method stub
		if((o.income-o.outcome)<(o1.income-o1.outcome)){
			return -1;
			
		}
		else if((o.income-o.outcome)>(o1.income-o1.outcome)){
			return 1;
			
		}
		else{
			return 0;
		}
	
	}
}
