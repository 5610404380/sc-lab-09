package model;


import java.util.ArrayList;

import interfaces.Taxable;

public class TaxCalculator {
	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0;
		for (Taxable t: taxList){
			sum = sum + t.getTax();
		}
		return sum;
		
	}
}
