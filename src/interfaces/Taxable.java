package interfaces;

public interface Taxable {
	double getTax();
}
