package control;

import java.util.ArrayList;
import java.util.Collections;

import model.TaxComparator;
import model.EarningComparator;
import model.ExpenseComparator;
import model.ProfitComparator;
import interfaces.Measurable;
import interfaces.Taxable;
import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;

public class TestCase {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestCase testcase = new TestCase() ;
		/*testcase.testPer() ;
		System.out.println("\n") ;
		testcase.testBCPmin() ;
		System.out.println("\n") ;
		testcase.testTax() ;
		System.out.println("\n") ;*/
		testcase.testper() ;
		System.out.println("\n") ;
		testcase.testpro() ;
		System.out.println("\n") ;
		testcase.testcom() ;
		System.out.println("\n") ;
		testcase.testTaxCompare() ;
	}
	public void testPer(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Make",173,3950000);
		persons[1] = new Person("Mark",175,50);
		persons[2] = new Person("Noey",154,4753600);

		System.out.println("Average tall : " + Data.average(persons));
	}
	public void testBCPmin(){
		Measurable[] tomin = new Measurable[3];
		Measurable[] persons = new Measurable[6];
		persons[0] = new Person("Make",173,3950000);
		persons[1] = new Person("Noey",154,4753600);
		persons[2] = new BankAccount("Thaipanit",3950000);
		persons[3] = new BankAccount("Kasikorn",50);
		persons[4] = new Country("Thai",4753600);
		persons[5] = new Country("England",50);
		
		tomin[0] =  Data.min(persons[0],persons[1]);
		tomin[1] =  Data.min(persons[2],persons[3]);
		tomin[2] =  Data.min(persons[4],persons[5]);
		System.out.println("Min tall : "+tomin[0].getMeasure());
		System.out.println("Min Bank : "+tomin[1].getMeasure());
		System.out.println("Min Country : "+tomin[2].getMeasure());
		
	}
	public void testTax(){
		ArrayList<Taxable> per = new ArrayList<Taxable>();
		ArrayList<Taxable> com = new ArrayList<Taxable>();
		ArrayList<Taxable> pro = new ArrayList<Taxable>();
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		
		per.add(new Person("a",150,300000));
				
		com.add(new Company("Company",1000000,800000));
		
		pro.add(new Product("item",100));
		
		tax.add(new Person("a",150,300000));
		tax.add(new Company("Company1",1000000,800000));
		tax.add(new Product("item1",100));
		
		System.out.println("Person sum tax : "+TaxCalculator.sum(per));
		System.out.println("Company sum tax : "+TaxCalculator.sum(com));
		System.out.println("Product sum tax : "+TaxCalculator.sum(pro));
		System.out.println("All sum tax : "+TaxCalculator.sum(tax));
	}
	public void testper(){
	ArrayList<Person> persons = new ArrayList<Person>();
	persons.add(new Person("Rick", 185, 100000));
	persons.add(new Person("Glenn", 180, 500000));
	persons.add(new Person("Sasha", 175, 200000));
	Collections.sort(persons);
	for(Person p : persons){
		System.out.println(p);
}
}
	public void testpro(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("TV", 100000));
		products.add(new Product("Computer", 20000));
		products.add(new Product("Telaphone", 5000));
		Collections.sort(products);
		for(Product p : products){
			System.out.println(p.toString());
			
		}
	}
	public void testcom(){
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		companies.add(new Company("Google", 4000000, 200000));
		Collections.sort(companies,new EarningComparator());
		for(Company p : companies){
			System.out.println(p);
			
		}
		Collections.sort(companies,new ExpenseComparator());
		for(Company p : companies){
			System.out.println(p);
			
		}
		Collections.sort(companies,new ProfitComparator());
		for(Company p : companies){
			System.out.println(p);
			
		}
	}
	public void testTaxCompare(){
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 500000));
		
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		
		ArrayList<Taxable> allElements = new ArrayList<Taxable>();
		allElements.addAll(persons);
		allElements.addAll(companies);
		allElements.addAll(products);
		Collections.sort(allElements,new TaxComparator());
		for(Taxable p : allElements){
			System.out.println(p);
			
		}
		
	}
}